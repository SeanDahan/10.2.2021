#include "MTServer.h"
#include "Helper.h"
#include <exception>
#include <iostream>
#include <string>


MTServer::MTServer()
{
	// this server uses TCP and that's why we use SOCK_STREAM & IPPROTO_TCP
	// if the server uses UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	this->_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

MTServer::~MTServer()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that were allocated in the constructor
		closesocket(this->_serverSocket);
	}
	catch (...) {}
}

void MTServer::SetupServe(int designatedPort)
{
	struct sockaddr_in sockAddress = { 0 };

	sockAddress.sin_port = htons(designatedPort);  // initializing the port which the socket listens in
	sockAddress.sin_family = AF_INET;  // must be AF_INET
	sockAddress.sin_addr.s_addr = INADDR_ANY;  // when there are few ip's for the machine. We will always use "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sockAddress, sizeof(sockAddress)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients (SOMAXCONN means talk to as much clients as the server can)
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

	std::cout << "Listening on port " << designatedPort << std::endl;

	// create a thread which will write the conversation history between users to their designated log file
	std::thread logFileWriterThread(&MTServer::writeConversationIntoLog, this);
	logFileWriterThread.detach();  // tell the log writing thread to work independently (detaching it from the calling thread)

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;

		acceptClient();
	}
}

void MTServer::acceptClient()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	// if the created conversation socket is invalid, throw an exception
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;

	// create a thread for the client and let it execute a function that handles the client
	std::thread clientHandlerThread(&MTServer::handleClient, this, client_socket);

	// don't wait for the thread to finish working, keep listening for another request
	clientHandlerThread.detach();
}

void MTServer::handleClient(SOCKET clientSocket)
{
	try
	{
		// first part of the message: 
		//getting the message's type code (should be 200 - a login message code)
		int msgType = Helper::getMessageTypeCode(clientSocket);

		// second part of the message:
		// getting the length of the username (this part is 2 bytes long)
		int usernameLength = Helper::getIntPartFromSocket(clientSocket, USERNAME_LENGTH_PART_SIZE);

		// third part of the message:
		// getting the username itself (using the length of the username which we received previously)
		std::string usernameStr = Helper::getStringPartFromSocket(clientSocket, usernameLength);
		this->_serverMutex.lock();
		this->_listOfUsernames.push_back(usernameStr);  // add the username to the queue of usernames
		this->_serverMutex.unlock();
		// send an update message to the client (contains the list of currently logged in users)
		Helper::send_update_message_to_client(clientSocket, "", "", concatenateLoggedUsers());

		while (true)
		{
			// first part of the message: 
			//getting the message's type code (should be 204 - a client update message code)
			msgType = Helper::getMessageTypeCode(clientSocket);

			// second part of the message:
			// getting the length of the username we want to send a message to (this part is 2 bytes long)
			usernameLength = Helper::getIntPartFromSocket(clientSocket, USERNAME_LENGTH_PART_SIZE);

			// third part of the message:
			// getting the username string of the user we want to send a message to
			std::string secondUsernameStr = Helper::getStringPartFromSocket(clientSocket, usernameLength);

			// fourth part of the message:
			// getting the length of the message itself that we want to pass to the second user (whose name we just received)
			int newMsgLength = Helper::getIntPartFromSocket(clientSocket, MSG_LENGTH_PART_SIZE);

			// fifth part of the message:
			// getting the message string that we want to send to the second user
			std::string msgContent = Helper::getStringPartFromSocket(clientSocket, newMsgLength);

			// if the username and message content fields are not empty then we can send the message
			if (secondUsernameStr != "" && msgContent != "")
			{
				// forming the message packet that we are sending to the other user
				msgContent = "&MAGSH_MESSAGE&&Author&" + usernameStr + "&DATA&" + msgContent;

				// form the name of the conversation's log file
				std::string conversationLogFileName = min(usernameStr, secondUsernameStr) + "&" + max(usernameStr, secondUsernameStr) + ".txt=" + msgContent;
				this->_queueOfMsgs.push(conversationLogFileName);  // add the name of the log file to the queue of log file names

				this->_cvLock.notify_one();  // notify the other thread that a log file to handle and write to was added
			}

			readLogFileAndSendUpdateMsg(clientSocket, usernameStr, secondUsernameStr);
		}
		closesocket(clientSocket);  // close the conversation socket between the server and the client
	}

	// if an unexpecteed exception is thrown, close the conversation socket
	catch (const std::exception& exception)
	{
		closesocket(clientSocket);
	}
}

void MTServer::readLogFileAndSendUpdateMsg(SOCKET clientSocket, std::string firstUsername, std::string secondUsername)
{
	// because we are reading from the log file and don't want the file's content to change in the
	// middle, we lock our unique lock
	std::unique_lock<std::mutex> conversationUniqueLock(this->_serverMutex);

	// form the name of the log text file that stores the conversation between the 2 users
	std::string conversationLogFileName = min(firstUsername, secondUsername) + "&" + max(firstUsername, secondUsername) + ".txt";
	// open the log file for reading
	std::ifstream inputFile(conversationLogFileName);

	std::string inputFileContent = "";
	std::string curLine = "";

	// read the file line by line and store it the in the input file content string
	while (std::getline(inputFile, curLine))
	{
		inputFileContent += curLine + "\n";
	}
	// remove the last character from the file content string (an unnecessary '\n')
	inputFileContent = inputFileContent.substr(0, inputFileContent.length() - 1);

	// send an update message to the client
	Helper::send_update_message_to_client(clientSocket, inputFileContent, secondUsername, concatenateLoggedUsers());

	inputFile.close();  // close the conversation log file
}

std::string MTServer::concatenateLoggedUsers()
{
	std::string usernamesString = "";

	// going through every username in the list and forming a string of all the usernames serperated by '&'
	for (std::list<std::string>::iterator it = this->_listOfUsernames.begin(); it != this->_listOfUsernames.end(); it++)
	{
		usernamesString += *it + "&";
	}
	// deleting the last character in the string (because we don't want the string to end with a '&')
	usernamesString = usernamesString.substr(0, usernamesString.length() - 1);

	return usernamesString;
}

void MTServer::writeConversationIntoLog()
{
	while (true)
	{
		std::mutex myLocalMutex;
		std::unique_lock<std::mutex> myLocalUniqueLocal(myLocalMutex);

		// wait for messages to be inserted to the queue by the other thread
		this->_cvLock.wait(myLocalUniqueLocal);

		while (!this->_queueOfMsgs.empty())
		{
			// getting the current message string to write in the log file (contains the users and the content of the message)
			std::string cuMsgStrToWrite = this->_queueOfMsgs.front();

			// slice the usernames involved in the conversation out of the current message string
			std::string usernamesInConversation = cuMsgStrToWrite.substr(0, cuMsgStrToWrite.find("="));
			// slice the message that was sent out of the current message string
			std::string curNewMsg = cuMsgStrToWrite.substr(cuMsgStrToWrite.find("=") + 1);

			// open the log file (the name of the conversation's log file is the usernames involved in the conversation)
			// we want to append the file content (add the new message to it without overwriting the file)
			std::ofstream conversationLogFile(usernamesInConversation, std::ios::app);

			conversationLogFile << curNewMsg;  // write the message to the log file

			conversationLogFile.close();  // close the log file
			this->_serverMutex.lock();
			this->_queueOfMsgs.pop();  // pop the message (we handled the current message, move to the next one)
			this->_serverMutex.unlock();
		}
	}
}