#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <queue>
#include <list>
#include <condition_variable>
#include <fstream>

#define USERNAME_LENGTH_PART_SIZE 2
#define MSG_LENGTH_PART_SIZE 5

class MTServer
{
public:
	// a constructor method to construct a multi threading server
	MTServer();

	// a destrctor method used to free all of the server's resources
	~MTServer();

	/*
		The function sets up the server (binds and configures the socket) and starts listening
		for incoming client requests.
		input:
			designatedPort - the port which the server listens on
		output:
			None
	*/
	void SetupServe(int designatedPort);

	/*
		The function accepts a request from a client creates a conversation socket and a thread
		for it. The created thread will begin the conversation between the client and the server
		without having to wait for them to finish.
		input:
			None
		output:
			None
	*/
	void acceptClient();

	/*
		The function handles the conversation with the client.
		input:
			clientSocket - a conversation socket that connects the server to the client we handle
		output:
			None
	*/
	void handleClient(SOCKET clientSocket);

	/*
		The function reads checks the currently logged in users (to find out if new users logged in or
		old users logged out) and if new messages were sent (when a message is sent, we need to send a server
		update message), and calls a function to send an appropriate server update message.
		input:
			clientSocket - a conversation socket that connects the server to the client
			firstUsername - the username of the first user that takes part in the conversation
			secondUsername - the username of the second user that takes part in the conversation
		output:
			None
	*/
	void readLogFileAndSendUpdateMsg(SOCKET clientSocket, std::string firstUsername, std::string secondUsername);

	/*
		The function creates and returns a string that is formed out of all the
		usernames in the username queue serperated by '&'.
		input:
			None
		output:
			a string of all the usernames in the queue serperated with '&'
	*/
	std::string concatenateLoggedUsers();

	/*
		The function writes the content of a conversation into it's specific log text file.
		input:
			None
		output:
			None
	*/
	void writeConversationIntoLog();


private:
	SOCKET _serverSocket;

	std::list<std::string> _listOfUsernames;

	std::queue<std::string> _queueOfMsgs;

	std::condition_variable _cvLock;

	std::mutex _serverMutex;
};