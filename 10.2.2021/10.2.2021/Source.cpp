#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "MTServer.h"
#include <iostream>
#include <exception>

int main()
{
	try
	{
		WSAInitializer wsaInit;
		MTServer myMagshiWhatsappServer;

		myMagshiWhatsappServer.SetupServe(8826);
	}
	catch (std::exception& error)
	{
		std::cout << "Error occured: " << error.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}